
public class Restar {
	int x, y;

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public int resta(int num1, int num2) {
		
		int result = 0;
		
		result = num1 - num2;
		
		return result;
		
	}
	
}
