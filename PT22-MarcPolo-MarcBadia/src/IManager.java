
public interface IManager {
	
	public void open();
	
	public void close();
	
	public void reset();
	
	public void guarda(Client c);
	
	public Client llegeix(int id);
}
