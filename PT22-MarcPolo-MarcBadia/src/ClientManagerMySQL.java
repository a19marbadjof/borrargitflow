import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ClientManagerMySQL implements IManager{
	
	Connection con = null;
	
	public void open() {
		try {
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/pt21", "marc", "12345");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void close() {
		try {
			if (con !=null) {
				con.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void reset() {
		
		String DROP_TAULA = "DROP TABLE IF EXISTS CLIENTS";
		String CREAR_TAULA = "CREATE TABLE CLIENTS(ID INT PRIMARY KEY, NOM VARCHAR(50), EDAT INT)";
		
		try (Statement sta = con.createStatement()) {
			sta.executeUpdate(DROP_TAULA);
			sta.executeUpdate(CREAR_TAULA);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		guarda(new Client(1, "Anna", 11));
		guarda(new Client(2, "Bea", 22));
		guarda(new Client(3, "Carles", 33));
	}
	
	public void guarda(Client c) {
		String INSEREIX = "INSERT INTO CLIENTS VALUES(" + c.getId() + ",'" + c.getNom() + "'," + c.getEdat() + ")";
		
		try (Statement sta = con.createStatement()){
			sta.executeUpdate(INSEREIX);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public Client llegeix(int id) {
		Client c = null;
		
		String CONSULTA = "SELECT * FROM CLIENTS WHERE ID = " + id;
		
		try (Statement sta = con.createStatement()){
			ResultSet rs = sta.executeQuery(CONSULTA);
			
			if (rs.next()) {
				String nom = rs.getString(2);
				int edat = rs.getInt("EDAT");
				c = new Client(id, nom, edat);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return c;
	}
}
