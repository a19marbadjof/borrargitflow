import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class ObjectDBManager implements IManager {

	EntityManagerFactory emf = null;
	EntityManager em = null;

	public void open() {

		emf = Persistence.createEntityManagerFactory("$objectdb/db/p2.odb");
		em = emf.createEntityManager();
	}

	@Override
	public void close() {
		em.close();
		emf.close();
	}

	@Override
	public void reset() {
		em.getTransaction().begin();
		em.createQuery("DELETE FROM Client").executeUpdate();
		em.getTransaction().commit();
		guarda(new Client(1, "Euclideo", 55));
		guarda(new Client(2, "Jerifrasia", 15));
		guarda(new Client(3, "Jesi", 88));
		guarda(new Client(4, "Gomera", 73));
		guarda(new Client(5, "Biggus Dicus", 38));
	}

	@Override
	public void guarda(Client c) {
		
		em.getTransaction().begin();
		em.persist(c);
		em.getTransaction().commit();
	}

	@Override
	public Client llegeix(int id) {
		TypedQuery<Client> querry= em.createQuery("SELECT c FROM Client c WHERE c.id="+id,Client.class);
		Client c = querry.getSingleResult();
		return c;
	}

}
